# Single Page Spa del Alma
# DOMAIN: spadelalma.com.ve

Single page realizada con fines comerciales para la empresa Spa del Alma, F.P. ubicada en Venezuela y Ecuador.

## Autores del Proyecto

Programación: Gerald Leonel Alarcón

## Ejecutar el proyecto en modo desarrollo

Para ejecutar el proyecto, debe descargar el repositorio e instalar las dependencias que están dentro de las carpetas /Server y /webpack. Una vez hecho el ```npm install``` dentro de cada uno de ellos, se pone en modo de desarrollo con los comandos ```npm start``` en la carpeta /Server y ```npm run webpack: watch``` en la carpeta /webpack. 

Puede visualizarse correctamente en el puerto 3000.

# ENGLISH 

# Single Page Spa del Alma
# DOMAIN: spadelalma.com.ve

Single page made for commercial purposes for the company Spa del Alma, F.P. located in Venezuela and Ecuador.

## Project Authors

Programming: Gerald Leonel Alarcón

## Run the project in development mode

To run the project, you must download the repository and install the dependencies that are inside the /Server and /webpack folders. Once the ```npm install``` is done inside each of them, it is put into development mode with the ```npm start``` command in the /Server folder and ```npm run webpack: watch ``` in the /webpack folder.

It can be displayed correctly on port 3000.