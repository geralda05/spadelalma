var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var collector = express();

collector.use(bodyParser.urlencoded({
  extended:true,
}))

collector.use(bodyParser.json());
collector.use(cookieParser());
collector.use(express.static(path.join(__dirname, 'public')));

/* Conexión de la BD */
var connections = require('../configs/connection');

/* Leer Registros de la BD */
collector.get('/leer/:RegistroID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.RegistroID ? req.params.RegistroID:null),
        ]
        var SQL = (req.params.RegistroID ? "SELECT * FROM registros WHERE RegistroID=?":"SELECT * FROM registros");
        connections.taller6_actividad4.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Agregar a la BD */
collector.post('/agregar',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.RegistroNombre !== 'undefined'){

        let Request = [
            req.body.RegistroNombre,
            req.body.RegistroEmail,
            req.body.RegistroTelefono,
            req.body.RegistroMensaje,
        ]
        var SQL = "INSERT INTO registros (RegistroNombre,RegistroEmail,RegistroTelefono,RegistroMensaje) VALUES (?,?,?,?)";
        connections.taller6_actividad4.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})


/* Función de Modificar en la BD */
collector.post('/modificar/:RegistroID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.RegistroNombre !== 'undefined'){

        let Request = [
            req.body.RegistroNombre,
            req.body.RegistroEmail,
        ]
        var SQL = "UPDATE registros SET RegistroNombre=? , RegistroEmail=? WHERE RegistroID='"+req.params.RegistroID+"'";
        connections.taller6_actividad4.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Eliminar en la BD */
collector.get('/eliminar/:RegistroID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM registros WHERe RegistroID='"+req.params.RegistroID+"'";
        connections.taller6_actividad4.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

module.exports = collector;