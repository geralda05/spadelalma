import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import index from './views/index.vue'
import inicio from './views/templates/inicio/inicio.vue'
import servicios from './views/templates/pages/servicios.vue'
import services from './views/templates/pages/services.vue'
import personalinfo from './views/templates/pages/personalinfo.vue'
import contact from './views/templates/pages/contact.vue'
import talleres from './views/templates/pages/talleres.vue'

const router = new VueRouter({
    mode:'history',
    routes:[
        {
            path:'/',
            redirect: "/inicio",

        },
        {
            path:'/inicio',
            name:"inicio",
            component: inicio,
        },
        {
            path:'/servicios',
            name:"servicios",
            component: services,
            children:[
                {
                    path:'/',
                    name:"services",
                    component: servicios,
                },
                {
                    path:'talleres',
                    name:"talleres",
                    component: talleres,
                },        
            ]
        },
        {
            path:'/contact',
            name:"contact",
            component: contact,
        },
        {
            path:'/personalinfo',
            name:"personalinfo",
            component: personalinfo,
        },
        {
            path:"*",
            redirect:"/inicio",
        }
    ]
});

const app = new Vue({
    router,
    render: createEle => createEle(index)

}).$mount('#app');